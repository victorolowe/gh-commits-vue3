import { mount } from "@vue/test-utils";
import List from "@/components/List.vue";

const mockListProps = {
  title: "Repositories",
  loadingMessage: "Loading...",
  emptyMessage: "No repositories are available",
  errorMessage: "",
};

const emptyMockListProps = { ...mockListProps, items: [] };
const sampleMockListProps = { ...mockListProps, items: ["Sample"] };
const listLoadingData = { loading: true };
const listLoadedData = { loading: false };

describe("List.vue", () => {
  it("renders a correct title", () => {
    const wrapper = mount(List, { props: emptyMockListProps });
    const listTitle = wrapper.get('[data-test="list-title"]');

    expect(listTitle.text()).toEqual(emptyMockListProps.title);
  });

  it("renders a correct loading message", () => {
    const wrapper = mount(List, {
      props: emptyMockListProps,
      data() {
        return {
          ...listLoadingData,
        };
      },
    });
    const listLoadingMessage = wrapper.get(
      '[data-test="list-loading-message"]'
    );

    expect(listLoadingMessage.text()).toEqual(
      emptyMockListProps.loadingMessage
    );
  });

  it("renders a correct empty message", () => {
    const wrapper = mount(List, {
      props: emptyMockListProps,
      data() {
        return {
          ...listLoadedData,
        };
      },
    });
    const listEmptyMessage = wrapper.get('[data-test="list-empty-message"]');

    expect(listEmptyMessage.text()).toEqual(emptyMockListProps.emptyMessage);
  });

  it("renders a correct error message", () => {
    const testErrorMessage = "Error";

    const wrapper = mount(List, {
      props: { ...emptyMockListProps, errorMessage: testErrorMessage },
      data() {
        return {
          ...listLoadedData,
        };
      },
    });
    const listErrorMessage = wrapper.get('[data-test="list-error-message"]');

    expect(listErrorMessage.text()).toEqual(testErrorMessage);
  });

  it("does not render a list when there are no items to display", () => {
    const wrapper = mount(List, {
      props: emptyMockListProps,
      data() {
        return {
          ...listLoadedData,
        };
      },
    });

    expect(wrapper.find('[data-test="list-wrapper"]').exists()).toBe(false);
  });

  it("renders a list when there are items to display", () => {
    const wrapper = mount(List, {
      props: sampleMockListProps,
      data() {
        return {
          ...listLoadedData,
        };
      },
    });

    expect(wrapper.find('[data-test="list-wrapper"]').exists()).toBe(true);
  });

  it("renders list items in the slot", () => {
    const wrapper = mount(List, {
      props: sampleMockListProps,
      slots: {
        default: "List Content",
      },
      data() {
        return {
          ...listLoadedData,
        };
      },
    });

    expect(wrapper.find('[data-test="list-wrapper"]').text()).toContain(
      "List Content"
    );
  });
});
