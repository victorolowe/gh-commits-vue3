import { mount } from "@vue/test-utils";
import Header from "@/components/Header.vue";

describe("Header.vue", () => {
  it("renders a correct title", () => {
    const title = "nasa";

    const wrapper = mount(Header);
    const headerTitle = wrapper.get('[data-test="header-title"]');

    expect(headerTitle.text()).toEqual(title);
  });
});
