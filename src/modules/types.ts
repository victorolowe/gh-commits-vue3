export interface IRepository {
  id: string;
  name: string;
  description: string;
  language: string;
  stars: number;
  updatedAt: Date;
}

export interface ICommit {
  sha: string;
  message: string;
  committer: string;
  date: Date;
  committerAvatar: string | null;
}
