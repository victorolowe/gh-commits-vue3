export interface Api {
  ghFetch: (input: string) => Promise<Response>;
}

const api: Api = {
  async ghFetch(input: string): Promise<Response> {
    return await fetch(`https://api.github.com/${input}`);
  },
};

export default api;
